# Introducción

Edge (Edge Technology S.A.) es una empresa que brinda solutiones the media streaming a empresas de de medios y relacionados. Nuestros clientes van de canales de televisión a cable operadores.

Para satisfacer las demandas de un mercado cambiante y con muchas aristas, es importante enteder y tratar de cubrir al máximo las necesidades de nuestros clientes. Para ello es necesario contar con buena información en tiempo real para obtener un buen panorama acerca del estado y calidad de nuestra solución (QA).

> Recuerde que este documento es confidencial y no debe ser compartido con nadie que no esté participando de este proceso de contratación.

## Objectivos

El presente estudio de caso tiene por objectivo evaluar las capacidades del ponstulante y trasmitir una situación tipica del puesto a través del planteo de un problema tipo.

## Desarrollo

Como se aclara en los obejtivos, es deseable que el postulante sea capaz de plantear una solución al problema que se plantea, pero dado que se trata de un puesto de programador, también necesitamos contar con código escrito y funcinal para poder evealuar:

* Calidad del código.
* Uso de comentarios.
* Seleccion de dependencias.
* Uso de herramientas (GIT, UML, MD, etc).
* Estructura del proyecto.
* Casos de testeo (unit-test).
* Visión de general de la architectura de la solución.

## Recursos

A continuación se lista una serie de recursos (leguajes, apis, tools) que son las que preferiríamos que se utilizaran en la entrega. Son las herramientas que nosotro utilizamos y que el postulante utilizará en el puesto una vez que ingrese.

### VideoJS

Video.js es un video player para web construido  en base a HTML5. Video JS es parte fundamental de la plataforma de Streaming de EDGE. Es nuestro player por defecto y sobre este player construimos nuestras soluciones.

[https://videojs.com/](https://videojs.com/)
[https://docs.videojs.com/](https://docs.videojs.com/)

### NodeJS

Si el postulante decide incluir procesos, workers, funciones, etc. preferimos que lo haga con la última version estable de Node JS (14+).

[Nodejs 14+](https://nodejs.org/dist/latest-v14.x/docs/api/)

### Typescript

TypeScript es un lenguaje de programación de código abierto desarrollado y mantenido por Microsoft. Es un superconjunto (superset) de JavaScript, que esencialmente añade tipos estáticos y manejo de clases y herencias.

EDGE ha incorporado typscript como standar para escribir aplicaciones compliadas tanto para HTML5 como para NodeJs o similares.

[https://www.typescriptlang.org/](https://www.typescriptlang.org/)

### WebPack

Webpack es una herramienta que permite crear code bundles para distribuir código en la web.

[https://webpack.js.org/](https://webpack.js.org/)

# Planteo

## Parte 1

Diseñar una interface que nos permita instanciar el player, y cargar la configuración del player. 

Para simplificar definiremos el siguiente configuration bundle:

```json
{
	"playerMode": "video",
	"monetization": null,
	"behaviour": {
	    "autoPlay": false
	},
	"theme": {
	    "mode": "vjs",
	    "id": "Forest"
	},
	"analytics": {
        "edge":true,
        "GA": {}
	},
	"feed": {
	    "mode": "live",
	    "assetId": "hjk-876-ghf-786",
	    "forceSource": {
	        "url": "http://localhost:8081/master.html"
	    }
	},
	"presentation": {
	    "poster": {
	        "url": "http://localhost:8081/poster.jpg"
	    }
	}
}
```

Esta objeto de configuración será utilizado para definir los parámetros de instaciación (setup) del player de EDGE. Tener en cuenta que este objeto configura el player de EDGE, no VidejoJS.

### Requerimientos

* El player debe ser contenido en un iFrame, similar a como funciona el player de youtube, y otras interfaces similares (Google IMA, por ejemplo).
* El iframe tiene que poder ser instanciado por código utilizando la API.
* El player (en el iframe) tiene que poder poder recibir las siguientes instrucciones:
    * setup 
    * play
    * pause
    * stop (pausa y resetea al status)
    * mute
* La API tiene que poder recibir eventos del player:
    * onPause
    * onPlay
    * onError
    * onBuffering
    * onVolumeChange
* La API tiene que poder acceder al status del player utilizando uan interface que incluya, por ejemplo:
    * getVolume
    * isPlaying
    * isReady

#### Componetes

Pude pensarse el la integración del plaqye de la siguiente forma:

![lista de componentes](./doc-res/components.png)

Los script que controlan la web crean una instancia del player utiliazndo las interfaces provistas por la API del Edge Player. Esa API embebe un iframe en la web y de alguna manera emite un mensaje al iframe para indicar que debe renderizarse el player.

Dentro del iframe existe uncontrollador que recibe esos mensages y renderiza un VideoJS, que asubes controla a traves del canal de mensajería contra la web que lo hostea.

### Entregarbles

* Architectura de alto nivel (high-level architecture) que describa los componentes involucrados y sus conexiones.
    * Incluir un diagrama que ayude a interpretar la solución.
    * Incluir un diagrama del ciclo de vida del player.
* Descripción de la API, eventos, métodos, etc.

## Parte 2

Implementar la solución propuesta como entregable de la parte uno. El enunciado incluye código base que puede ser utilizado como guía.

### Requerimientos

* El código debe correr y funcionar.
* Agregar documentación y comentarios que permitan ayudar a interpretar el código.
* Incluir las instrucciones de ejecución en la documentación.
* Incluir los escenarios de pruebas: pensar y enunciar las condiciones de borde, etc.

## Parte 3

### Parte 3-A

Para poder obtener información inmediata del estado del player, e incluso como fuente para compilar resúmenes y reportes, el player emite mensajes hacia la plataforma de estadísticas en la nube.

Programar un plugin de VideoJS que permita capturar los eventos del player y enviara a la plataforma de estadísticas los siguientes eventos:

*REDAY_TO_PLAY (Enviado cuando el player termina el setup y puede comenzar a reproducir)
*FIRSTPLAY (Se envía ciando el player reproduce el video por primera vez)
*PLAYING (Se envía cada 55 segundos si el player está reproduciendo)
*STOP (Se envía cuando el player recibe la instrucción de stop)
*PAUSE (Se envía cuando el player recibe la instrucción de pause)
*PLAY (Se envía cuando el player recibe la instrucción de play y no es la primero reproducción)

### Requerimientos
* Estructura general del código.
* Pensar en los escenario de test y proponer (no implementar) una solución de testo automático.
* **Bonus**: Implementar el plugin y agregarlos al código las partes 1 y 2.

### Parte 3-B
En el bundle de configuración  en la parte 1, existe un objet que no está descripto:  `analytics.GA`. Dicho objeto tiene como función definir una integración con google analytics. 

Proponer una estructura de datos que permita en forma conveniente configurar una cuenta de Google Analytic utilizando Tag manager.
[Deploy Universal Analytics with Tag Manager - Tag Manager Help (google.com)](https://support.google.com/tagmanager/answer/6107124?hl=en)

> TIP: No pierda el tiempo con  una solución rebuscada, haga completo y simple.
 
## Bonus
### Bonus 1

Integrar la solución con Webpack para generar código distribuidle.

### Bonus 2

Programar los tests  para los escenarios propuestos en la parte 2.

# Condiciones de entrega

Enviar un correo electrónico a con una carpeta comprimida en formato ZIP incluyendo el enunciado y la solución. Incluir también una referencia al nombre del author en el nombre por conveniencia.

Enviar la solución una semana (5 días hábiles) después de recibido el enunciado, es decir, si lo recibió un lunes, a mas tardar debe enviarlo el martes de la siguiente semana antes del mediodía.

Utilice la siguiente dirección de correo electrónico: [pablo@edge-americas.com](mailto:pablo@edge-americas.com).

> No importa si no logra completar la totalidad del test, es preferible que cuide la calidad y el planteo de la soluciona a cubrir el 100% del problema.  Luego de recibir la resolución se realizará una session virtual (remota) para repasar y defender la solución.

# Confidencialidad

Este documento es confidencial, le pedimos que no lo comparta y que no comparta tampoco, detalles del proceso con nadie que sea ajeno a Edge Technology S.A.












