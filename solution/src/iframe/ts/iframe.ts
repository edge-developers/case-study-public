(function main(){

    let setupData = {
        controls: true,
        autoplay: true
    }

    let player = videojs(document.getElementById("player"), setupData)
    
    player.src({
        src: '/hls/play.m3u8',
        type: 'application/x-mpegURL'
    });

})();