class EdgePlayer{

    private iframe = document.createElement("iframe");
    private div:Element = null;

    constructor(divId: string){
        
        let div = document.getElementById(divId);

        if(!div){
            console.error("Div not existant! Plase verify your div ID.");
        }

        this.div = div;
     
        this.preload().then(()=>{
           console.log("iframe load complete");
        });
       
    }

    private preload(){
        return new Promise<void>((resolve, reject) => {
            this.iframe.onload = () => {
                resolve()
            }
    
            this.iframe.setAttribute("frameborder","0");
            this.iframe.setAttribute("allow","autoplay, fullscreen");
            this.iframe.setAttribute("allowfullscreen","");
            this.iframe.setAttribute("width","100%");
            this.iframe.setAttribute("height","100%");
    
            this.iframe.setAttribute("src","/iframe");
    
            this.div.appendChild(this.iframe);
        });
    }

}