import express from "express"
import { route as liveSim } from "./lib/live-simulator.js";
import {join, dirname} from "path";

const __dirname = dirname(import.meta.url.substr(5));

const app = express()
const port = 3000

app.use((req, res, next) => {
  res.set('Cache-Control', 'no-store')
  next()
});

app.use("/hls", liveSim);

app.use("/iframe", express.static(join(__dirname,"../dist/iframe")));
app.use("/api", express.static(join(__dirname,"../dist/api")));
app.use("/", express.static(join(__dirname,"../public-test")));


app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})