import copy from "copy";
import { basename } from "path";

export const SP = "  ";
export const SP2 = SP  + SP;
export const SP3 = SP2 + SP;
export const SP4 = SP3 + SP;
export const SP5 = SP4 + SP;


export async function cp(origin, dest) {
    return new Promise((resolve,reject)=>{
        copy(origin, dest, (err, files)=>{
            if(err){
                reject(err);
            }else{
                resolve(files);
            }
        });
    });
}

export function printFiles(files,prefix = ""){
    for(let file of files){
        console.log(prefix, basename(file.dest));
    }
}