import fs from "fs"
import {join, dirname} from "path";
import express from "express"

export const route = express.Router();

const __dirname = dirname(import.meta.url.substr(5));

const manifest = JSON.parse(fs.readFileSync(join(__dirname,"../ts/video.json")).toString());

const hls = {
    current:0,
    count: manifest.fragments.length,
    playlistLentgh: 4,
    fragments: [],
    manifest: ""
}

const state = {
    halted: false,
    freeze: false,
    _404: false
}

setInterval(updateHls,manifest.targetDuraction);
updateHls();

route.use("/fragments", express.static(join(__dirname,"../ts")));

route.get("/play.m3u8",(req, res)=>{

    if(state._404){
        res.status(404).send("Streaming offline");
    }else if(state.halted){
        res.send("#EXTM3U\n"+
        `#EXT-X-VERSION:3\n`+
        `#EXT-X-TARGETDURATION:0\n`+
        `#EXT-X-MEDIA-SEQUENCE:0\n`+
        `#EXT-X-ENDLIST`);
    }else{
        res.send(hls.manifest);
    }
})

route.get("/api/stream/resume",(req, res)=>{
    state.halted = false;
    state.freeze = false;
    state._404 = false;
    updateHls();

    console.log("Stream resumed");

    res.send();
});

route.get("/api/stream/freeze",(req, res)=>{
    state.halted = false;
    state.freeze = true;
    state._404 = false;
    updateHls();

    console.log("Stream freezed");

    res.send();
});

route.get("/api/stream/halt",(req, res)=>{
    state.halted = true;
    state.freeze = true;
    state._404 = false;
    updateHls();

    console.log("Stream halted");

    res.send();
});

route.get("/api/stream/404",(req, res)=>{
    state.halted = false;
    state.freeze = true;
    state._404 = true;
    
    updateHls();
    
    console.log("Stream stoped");

    res.send();
});


function updateHls(){

        if (state.freeze)
            return;
    
        let fragmentsBody = "";

        hls.fragments.push(manifest.fragments[hls.current%manifest.fragments.length]);
        if (hls.fragments.length > hls.playlistLentgh){
            hls.fragments.splice(0,1)
        }
    
        for(let fragment of hls.fragments){
            fragmentsBody += fragmentNotation("./fragments/" + fragment.fileName, fragment.duration, fragment.discontinuity);
        }
    
        hls.manifest = "#EXTM3U\n"+
        `#EXT-X-VERSION:3\n`+
        `#EXT-X-TARGETDURATION:10\n`+
        `#EXT-X-MEDIA-SEQUENCE:${hls.current}\n`+
        `${fragmentsBody}`;

        hls.current++

}

function fragmentNotation(path,duration, discontinuity){
    let text = "";

    if(discontinuity){
        text+= "#EXT-X-DISCONTINUITY\n";
    }

    text += `#EXTINF:${duration/1000},\n${path}\n`;
    return text;
}