import {cp,printFiles,SP,SP2,SP3} from "./lib/tools.js";
import {join, dirname} from "path";
import { execSync } from 'child_process';

const log = console.log;

const __dirname = dirname(import.meta.url.substr(5));

const IFRAME_RES = join(__dirname,"../src/iframe/template/");
const IFRAME_DEST = join(__dirname,"../dist/iframe/");
const IFRAME_TS = join(__dirname,"../src/iframe/");
const API_TS = join(__dirname,"../src/api/");

const VIDEOJS_DIST = join(__dirname,"../node_modules/video.js/dist/");
const VIDEOJS_CONTRIB_VLS_DIST = join(__dirname,"../node_modules/@videojs/http-streaming/dist/");

(async function main(){
    
    log("Compilando iframe...");
    
    log(SP,"Copiando template...");
    printFiles(await cp(join(IFRAME_RES,"*"), IFRAME_DEST), SP2);

    log(SP,"Copiando Video JS...");
    printFiles(await cp(join(VIDEOJS_DIST,"*"), join(IFRAME_DEST,"lib")), SP2);

    log(SP,"Copiando Video JS VLS (Contrib)...");
    printFiles(await cp(join(VIDEOJS_CONTRIB_VLS_DIST,"*"), join(IFRAME_DEST,"lib")), SP2);

    log(SP,"Compilando...");
    execSync("npx tsc", {cwd: IFRAME_TS});
    log(SP2,"...terminado");

    log();

    log("Compilando Player API...");
    execSync("npx tsc", {cwd: API_TS});
    log(SP2,"...terminado");


})();