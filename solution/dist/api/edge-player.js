var EdgePlayer = (function () {
    function EdgePlayer(divId) {
        this.iframe = document.createElement("iframe");
        this.div = null;
        var div = document.getElementById(divId);
        if (!div) {
            console.error("Div not existant! Plase verify your div ID.");
        }
        this.div = div;
        this.preload().then(function () {
            console.log("iframe load complete");
        });
    }
    EdgePlayer.prototype.preload = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.iframe.onload = function () {
                resolve();
            };
            _this.iframe.setAttribute("frameborder", "0");
            _this.iframe.setAttribute("allow", "autoplay, fullscreen");
            _this.iframe.setAttribute("allowfullscreen", "");
            _this.iframe.setAttribute("width", "100%");
            _this.iframe.setAttribute("height", "100%");
            _this.iframe.setAttribute("src", "/iframe");
            _this.div.appendChild(_this.iframe);
        });
    };
    return EdgePlayer;
}());
