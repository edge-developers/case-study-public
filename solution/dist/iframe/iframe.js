(function main() {
    var setupData = {
        controls: true,
        autoplay: true
    };
    var player = videojs(document.getElementById("player"), setupData);
    player.src({
        src: '/hls/play.m3u8',
        type: 'application/x-mpegURL'
    });
})();
