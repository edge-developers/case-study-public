> REEMPLAZAR EL CONTENIDO DE ESTE ARCHIVO CON LA DOCUMENTACION DEL CÓDIGO

La siguiente estructura es básic y sirve como puntapie para resolver el test.

# Compilar

Primero instale los paquetes necesarios:

    npm install

Luego compile el código:

    npm run compile

# Correr el código

Para ejecutar el código y realizar pruebas utilice:

    npm run serve

luego abra en un browser (Chrome o La ultima version de Miocrosoft Edge es recomendable):

[http://localhost:3000](http://localhost:3000)

# Test

Una vez que pueda correr el código correctamente debería ver algo asi:

![lista de componentes](./docs/test.jpg)


*  La sección de consola debería contener un log de los eventos del player que debe implemntar el o la postulante.
*  Los botones de la barra "Player Control" llaman a una de las posible funciones del player, debe ser implemntado por el o la postulante.
*  La sección "Streaming eventys simulation" contiene una serie de controles que permiten simulas las condiciones del streaming.

# Simulador de streaming.

El ejemplo contiene una simulacion de streaming en vivo. Cuando se inicia el código, el streming empieza a loopear (como si fuera un vivo) un stremaing en formato HLS.

La botonera permite simular eventos que permiten detonear erros en el player.

### RESUME

Resume el steraming conrmalmente.

### Freeze

Detiene el steramin, pero mantene el playlis. Permite simular un evento de buffering.

Al presionar freeze el playlist (ver especificacion del hls) se congela, al consumir el ultimo fragmento, el player comenzará un evento de buffering. Si luego de que esto ocurra se presiona resume, el player debería retomar la reproduccion en un máximo de 10 segundos (la duracion promeido de los fragmentos).

### Halt

Reemplaza el playlist por una lista vacía. Debería probocar que el player se detenca sin posibilidad de recuperación. Por supuesto que el postulante podría pensar en una solución para obtener cveeditos extra.

### 404

Es similar a Freeze, pero en vez de de congelar el playlist, el stream comezará a devolver status code 404. Sería interesante comprobar cual es la diferencia en el logg de errors del player.

# Nota final.


En un escenario real el iframe, la api y el sitio que embebe el player se encuentran en diferentes sitios y dominios. Para simplificar la prueba y a la vez evitar problemas con los CORS o modificar el archivo "hosts", se obtiene un efecto similar separando cada componente en diferentes paths.

Utilizando Express como proxy, el programa de prueba (serve.js) separa los tres sitios en 3 diferentes paths:

* `hls` -> contiene el simulador del vivo.
* `api` -> contiene la API que el cliente utilizaría.
* `iframe` -> contiene el sitio completo del iframe

Por ultimo el directorio Raiz contiene con código que utilizaría el cliente para embeber el código.

> Como ocurría en un caso real, `/dist` contiene los archivos de distribución. Mientras que los archivos de soporte para la prueba están separados en `/public-test`.

# VISUAL CODE

Visual code es un editor que está cobrando mucha popularidad. Se recomineda utilizar Visual Code e instalr los iguiente complementos que facilitarán la navegación y escribir código:

*   https://marketplace.visualstudio.com/items?itemName=ms-vscode.vscode-typescript-next
*   https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint
*   https://marketplace.visualstudio.com/items?itemName=DavidAnson.vscode-markdownlint


